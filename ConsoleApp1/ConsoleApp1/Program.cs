﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string inputStr = "";                       // Исходная строка
            char[] delimiters = new char[1];            // Разделитель
            string deliter = "";
            int n = 0;                                  // Номер символа в строке
            char Symbol = ' ';                          // Символ, которым нужно заменить n-ный символ в слове

            Console.Write("Введите текст на английском: ");
            inputStr = Console.ReadLine();

            for (int i = 0; i < delimiters.Length; i++)
            {
                Console.Write("Введите символ разделителя: ");
                deliter = Console.ReadLine();
                if (deliter.Length == 1)
                    delimiters[i] = Convert.ToChar(deliter);
                else
                    i = i - 1;
            }

            string nn = "1";
            for (int j = 0; j < nn.Length; j++)
            {
                Console.Write("Введите индекс заменяемого символа в слове: ");
                nn = Console.ReadLine();
                if (Char.IsDigit(nn[j]))
                    if (Convert.ToInt32(nn) > 0)
                    {
                        n += Convert.ToInt32(nn.Substring(j));
                    }
                    else
                    {
                        Console.WriteLine("Введите больше нуля!");
                        j = -1;
                    }
                else
                {
                    Console.WriteLine("Введите цифру!");
                    j = -1;
                }
            }

            char tempSym;

            for (int k = 0; k < 1; k++)
            {
                Console.Write("Введите символ, которым нужно заменить: ");
                tempSym = Convert.ToChar(Console.ReadLine());
                if (tempSym.ToString().Length < 2)
                    Symbol = tempSym;
                else
                {
                    Console.WriteLine("Вы ввели более одного знака. Попытайтесь еще раз...");
                    k = -1;
                }
            }

            Console.WriteLine("-------------------------------------------------");
            Preobrazovanie(inputStr, delimiters[0], n, Symbol);
            Console.ReadKey();
        }

        private static void Preobrazovanie(string s, char del, int number, char c)
        {
            string result = "";
            // Разбиваем строку на слова и помещаем каждое слово в массив отдельной строкой
            string[] s2 = s.Split(new[] { del });
            for (int i = 0; i < s2.Length; i++)
            {
                if (s2[i].Length > number)                          // Если длина слова больше введенного индекса, то продолжаем
                {
                    //char cOld = s2[i][number];
                    StringBuilder sb = new StringBuilder(s2[i]);
                    sb[number - 1] = c;
                    result += sb.ToString() + " ";
                }
            }
            Console.WriteLine(result);
        }
    }
}
